using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zero_Script : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private float Vida = 5;
    private bool Muerto = false;
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();

    }
    void Update()
    {
        //Debug.Log("vida: " + Vida);
        if (Muerto)
        {
            if (Vida <= 0)
                Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision2D)
    {

        if (collision2D.gameObject.layer == 6)
        {
            Vida--;
            Muerto = true;
        }
        if (collision2D.gameObject.layer == 7)
        {
            Vida -=2.5f;
            Muerto = true;
        }
        if (collision2D.gameObject.layer == 8)
        {

            Vida -= 5;
            Muerto = true;
        }
    }
}
